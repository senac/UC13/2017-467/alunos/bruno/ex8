
package br.com.senac.exemplo8.test;

import br.com.senac.exemplo8.Vendas;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class VendasTest {
    
    public VendasTest() {
    }
    
    @Test
    public void PrecoFinalVendasRJ(){
        
    Vendas rjvenda = new Vendas("Rio de Janeiro", "Telemark", 10000);
        assertEquals(1700, rjvenda.RJVendas(), 0.01);
    }
    
    @Test
    public void PrecoFinalVendasSP(){
        Vendas spvenda = new Vendas("São Paulo", "SuporteAssist", 15000);
        assertEquals(2700,spvenda.SPVendas(), 0.01);     
    }
    
    @Test
    public void PrecoFinalVendasMA(){
        Vendas mavendas = new Vendas("Maranhão", "TicketBank", 11500);
        assertEquals(1380,mavendas.MAVendas(), 0.01);
    }
    
    @Test
    public void PrecoFinalVendasES(){
        Vendas esvendas = new Vendas("Espírito Santo", "Tatical Vision", 13000);
        assertEquals(1300,esvendas.ESVendas(), 0.01);
    }
}
