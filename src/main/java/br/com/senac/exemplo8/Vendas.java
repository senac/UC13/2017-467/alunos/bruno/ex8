
package br.com.senac.exemplo8;


public class Vendas extends CalculadoraICMS{
    private String estado;
    private String compradores;
    private double preco;

    public Vendas(String estado, String compradores, double preco) {
        this.estado = estado;
        this.compradores = compradores;
        this.preco = preco;
    }
    
    public double RJVendas (){
    double impostoFinal = preco * RIODEJANEIRO;
    
    return impostoFinal;
    }
    
    public double SPVendas(){
        double impostoFinal = preco * SAOPAULO;
        
        return impostoFinal;
    }
    
    public double MAVendas(){
        double impostoFinal = preco * MARANHAO;
        
        return impostoFinal;
    }
    
    public double ESVendas(){
        double impostofinal = preco * ESPIRITOSANTO;
        
        return impostofinal;
    }
   
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCompradores() {
        return compradores;
    }

    public void setCompradores(String compradores) {
        this.compradores = compradores;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
    
}
